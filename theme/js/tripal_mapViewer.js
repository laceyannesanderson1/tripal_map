/* 
 * File: tripal_mapViewer.js
 * Obtain settings from MapViewer PHP form, call linkageGroup draw to display selected chromosomes.
 */

(function($) {
	Drupal.behaviors.tripal_mapViewerBehavior = {
	attach: function (context, settings) {

		$('#select_fieldset_mapViewer').once('select_fieldset_mapViewer', function() {

		// Reference chromosome	
		var geneticFeatures =			Drupal.settings.mapViewerDisplayJS.mapViewer_genetic_features;
		var mapName =					Drupal.settings.mapViewerDisplayJS.reference_map_name;        
		var linkageGroupName =			Drupal.settings.mapViewerDisplayJS.reference_linkage_group;        
		var markerHighlight =			Drupal.settings.mapViewerDisplayJS.reference_genetic_marker_highlight;        
		markerHighlight['marker_pos'] = parseFloat(markerHighlight['marker_pos']); // convert string to float for marker pos
		markerHighlight['marker_locus_id'] = markerHighlight['marker_locus_id']; 

		// Comparison chromosome
		var geneticFeaturesComparison =	Drupal.settings.mapViewerDisplayComparisonJS.mapViewer_genetic_features_comparison;
		var mapComparisonName =			Drupal.settings.mapViewerDisplayComparisonJS.comparison_map_name;        
		var linkageGroupComparisonName=	Drupal.settings.mapViewerDisplayComparisonJS.comparison_linkage_group;        
		var showComparison =			Drupal.settings.mapViewerDisplayComparisonJS.show_comparison;

		var markerTypeDisplayStates = Drupal.settings.mapViewerDisplayJS.marker_type_display_states;
		var markerTypeColorMap =      Drupal.settings.mapViewerDisplayJS.marker_type_color_map;

		var showRuler = 0;
		if ($("#show_ruler_mapViewer:checked").val() !== undefined) {
			showRuler = 1;
		}

		var showMarkerPos = 0;
		if ($("#marker_pos_mapViewer:checked").val() !== undefined) {
			showMarkerPos = 1;
		}

		var expandQTL = 0;
		if ($("#qtl_expand_mapViewer:checked").val() !== undefined) {
			expandQTL = 1;
		}

		var strMarkerTypeDisplayStates = "";
		if (Object.keys(markerTypeDisplayStates).length > 0) {
			strMarkerTypeDisplayStates = JSON.stringify(markerTypeDisplayStates);
		}

		var show = {Ruler: showRuler, MarkerPos: showMarkerPos, ExpandQTL: expandQTL, Comparison: showComparison};
		var markers = obtainMarkers(geneticFeatures, geneticFeaturesComparison, showComparison);
		var uniqueMarkers = markers[0];  
		var uniqueMarkersComparison = markers[1];

		// Find markers that contribute most to the dimensional extent of the chromosome view. 
		// Take all qtls if exist.
		var uniqueMarkersQTL = uniqueMarkers.filter(i => ((i.genetic_marker_type == 'QTL') || (i.genetic_marker_type == 'haplotype_block')));
		var filteredUniqueMarkers = uniqueMarkers.slice(0).filter(i => ((i.genetic_marker_type != 'QTL') && (i.genetic_marker_type != 'haplotype_block')));  // deep copy array
		// if displaying genetic_marker_name or the genetic_marker_locus_name 
		if (Drupal.settings.tripal_map.corresMarkerNameType == 'Genetic Marker Name') {
			// Sort non QTL markers by name length, keeping only the longest one. (QTL has separate lane and can uses abbreviated text so handle separately)
			filteredUniqueMarkers.sort((a, b) => parseFloat(b.genetic_marker_name.length) - parseFloat(a.genetic_marker_name.length));
		}
		else {
			filteredUniqueMarkers.sort((a, b) => parseFloat(b.genetic_marker_locus_name.length) - parseFloat(a.genetic_marker_locus_name.length));
		}
		if (filteredUniqueMarkers.length > 0) {
			uniqueMarkersQTL.push(filteredUniqueMarkers[0]);
		}
		filteredUniqueMarkers = uniqueMarkersQTL;
	    
		// Take all qtls if exist.
		var uniqueMarkersComparisonQTL = uniqueMarkersComparison.filter(i => ((i.genetic_marker_type == 'QTL') || (i.genetic_marker_type == 'haplotype_block')));
		var filteredUniqueMarkersComparison = uniqueMarkersComparison.slice(0).filter(i => ((i.genetic_marker_type != 'QTL') && (i.genetic_marker_type != 'haplotype_block')));  // deep copy array
		// if displaying genetic_marker_name or the genetic_marker_locus_name 
		if (Drupal.settings.tripal_map.corresMarkerNameType == 'Genetic Marker Name') {
			// Sort non QTL markers by name length, keeping only the longest one. (QTL has separate lane and can uses abbreviated text so handle separately)
			filteredUniqueMarkersComparison.sort((a, b) => parseFloat(b.genetic_marker_name.length) - parseFloat(a.genetic_marker_name.length));
		}
		else {
			filteredUniqueMarkersComparison.sort((a, b) => parseFloat(b.genetic_marker_locus_name.length) - parseFloat(a.genetic_marker_locus_name.length));
		}
		uniqueMarkersComparisonQTL.push(filteredUniqueMarkersComparison[0]);
		filteredUniqueMarkersComparison = uniqueMarkersComparisonQTL;

		// initialize marker data for dimensional calculations
		var markerDataCalcRet = initMarkerData(linkageGroupName, mapName, filteredUniqueMarkers, strMarkerTypeDisplayStates, markerTypeColorMap, showComparison, 
			linkageGroupComparisonName, mapComparisonName, filteredUniqueMarkersComparison);	    
		var markerDataCalc = markerDataCalcRet[0];

		var containerRet = createSVGContainer(show, mapName, filteredUniqueMarkers, linkageGroupName, markerHighlight, markerDataCalc,
				mapComparisonName, linkageGroupComparisonName, filteredUniqueMarkersComparison, showComparison);

		var svg = containerRet[0];
		var chrFrameRefWidth = containerRet[1];
		var pageFit = containerRet[2];
	    	
		//====  Initialize marker data with full marker set for drawing on the chromosome views.====
		var markerDataRet = initMarkerData(linkageGroupName, mapName, uniqueMarkers, strMarkerTypeDisplayStates, markerTypeColorMap, showComparison, 
			linkageGroupComparisonName, mapComparisonName, uniqueMarkersComparison);	    
				
		var markerData = markerDataRet[0];

		// Draw the save to png icon
		var pngFileName = 'MapViewer_' + mapName + '_' + linkageGroupName + '_vs_' + mapComparisonName + '_' + linkageGroupComparisonName + '.png'; 
		var pngIcon = createPngIcon(pngFileName, svg);

		markerData.findCorrespondences(mapName, linkageGroupName);

		svg = drawChromosomes(uniqueMarkers, mapName, linkageGroupName, markerHighlight, markerData, chrFrameRefWidth,
			uniqueMarkersComparison, mapComparisonName, linkageGroupComparisonName, showComparison, pageFit, show, svg, pngIcon);

		svg = drawLegend(markerData, svg, showComparison, pngIcon);

    });


	//=================================================================================================================================
	// Helper functions
	//=================================================================================================================================
	function obtainMarkers(geneticFeatures, geneticFeaturesComparison, showComparison) {
		// Obtain markers, comparison markers
		// be very careful when creating a unique list of markers as some have the same name, but different position. 
		var uniqueMarkers = [];
		var uniqueMarkersComparison = [];

		for (var key in geneticFeatures) {
			uniqueMarkers.push(geneticFeatures[key]);
		}

		// Obtain comparison marker data if enabled
		if (showComparison) {
			for (var key in geneticFeaturesComparison) {
				uniqueMarkersComparison.push(geneticFeaturesComparison[key]);
			}
		}
		var ret = [uniqueMarkers, uniqueMarkersComparison]; 
		return ret;
	};

	function initMarkerData(linkageGroupName, mapName, uniqueMarkers, strMarkerTypeDisplayStates, markerTypeColorMap, showComparison, 
		linkageGroupComparisonName, mapComparisonName, uniqueMarkersComparison) {

		var markerData = new configMapViewer.MarkerData(strMarkerTypeDisplayStates, markerTypeColorMap);

		var linkageGroupId = "lg";
		var mapId = uniqueMarkers[0].featuremap_id;
		if (!(linkageGroupName)) {
			linkageGroupName = "Null";
		}
		markerData.addLinkageGroup(uniqueMarkers, linkageGroupName, mapName, linkageGroupId, mapId, mapViewer.OrientationEnum.LEFT);

		if (showComparison) {
			var linkageGroupComparisonId = "lgComp";
			var mapComparisonId = uniqueMarkersComparison[0].featuremap_id;
			if (!(linkageGroupComparisonName)) {
				linkageGroupComparisonName = "Null";
			}
			markerData.addLinkageGroup(uniqueMarkersComparison, linkageGroupComparisonName, mapComparisonName, linkageGroupComparisonId, mapComparisonId, mapViewer.OrientationEnum.RIGHT);
		}
		    
		var ret = [markerData];
		return ret;
	};
		    	
	function createSVGContainer(show, mapName, uniqueMarkers, linkageGroupName, markerHighlight, markerData,
		mapComparisonName, linkageGroupComparisonName, uniqueMarkersComparison, showComparison) {

		// prepend drawing view container before the control panel PHP form
		var container =  "#select_fieldset_mapViewer";
		$(container).before('<div></div><div id ="select_fieldset_mapViewer_svg" width="100%"></div>');

		// clear the svg container first
		var svgFieldset = "#select_fieldset_mapViewer_svg";
		d3.select(svgFieldset).selectAll("svg").remove(); 

		var svgHeight = 625;
		var svgMaxWidth = 1100;
		var pageFit = false;

		var svg = d3.select(svgFieldset)
			.append("svg").attr("class", "TripalMap").attr("width", svgMaxWidth).attr("height", svgHeight);

		// set the initial Marker Position, the case where the page specifically highlights a marker
		var initMarkerHighlight = markerHighlight;

		// get the svg width
		var mapId = uniqueMarkers[0].featuremap_id;
		var chrFrameReference = new mapViewer.ChrFrame(mapViewer.OrientationEnum.LEFT, pageFit, show, mapName, mapId, 
			linkageGroupName, markerData, initMarkerHighlight);
		var chrFrameDimensions = chrFrameReference.calculateDimensions(svg);
		var chrFrameRefWidth = chrFrameDimensions["width"];
		var chrFrameRefHeight = chrFrameDimensions["height"];

		var chrFrameCompDimensions = {"width": 0, "height": 0};
		var chrFrameCompWidth = 0;
		var chrFrameCompHeight = 0;
		var chrFrameComparison = "";

		if (showComparison) {
			var mapComparisonId = uniqueMarkersComparison[0].featuremap_id;
			chrFrameComparison = new mapViewer.ChrFrame(mapViewer.OrientationEnum.RIGHT, pageFit, show, mapComparisonName, 
				mapComparisonId, linkageGroupComparisonName, markerData, initMarkerHighlight);
			chrFrameCompDimensions = chrFrameComparison.calculateDimensions(svg); 
			chrFrameCompWidth = chrFrameCompDimensions["width"];
			chrFrameCompHeight = chrFrameCompDimensions["height"];
		}

		// if a scrollbar is required, nest the svg in the div for scrolling
		var svgWidth = chrFrameRefWidth + chrFrameCompWidth + 200 + 35;
		var maxChrFrameHeight = chrFrameRefHeight > chrFrameCompHeight ? chrFrameRefHeight : chrFrameCompHeight;
		var pngHeight = 50;
		var legendHeight = 15;
		var svgTotalHeight = pngHeight + maxChrFrameHeight + legendHeight;
		var mvScroll = "mv_scroll";
		if (svgWidth > svgMaxWidth) {
			// scrolling is required so try to fit the page by adapting the qtl and polygon sizes
			pageFit = true;
			chrFrameReference = new mapViewer.ChrFrame(mapViewer.OrientationEnum.LEFT, pageFit, show, mapName, mapId, 
				linkageGroupName, markerData, initMarkerHighlight);
			chrFrameDimensions = chrFrameReference.calculateDimensions(svg);
			chrFrameRefWidth = chrFrameDimensions["width"];
			svgWidth = chrFrameRefWidth + chrFrameCompWidth + 200 + 35;

			d3.select(svgFieldset).selectAll("svg").remove(); 
			d3.select(svgFieldset)
				.append("div").attr("class", "TripalMap").attr("id", mvScroll);
			svg = d3.select("#"+mvScroll)
				.append("svg").attr("class", "TripalMap").attr("width", svgWidth).attr("height", svgTotalHeight);
				// try to fit the page by sending flag to drawing code
		}
		else {
			d3.select(svgFieldset).selectAll("svg").remove(); 
			svg = d3.select(svgFieldset)
				.append("svg").attr("class", "TripalMap").attr("width", svgMaxWidth).attr("height", svgTotalHeight);
		}

		var ret = [svg, chrFrameRefWidth, pageFit]; 
		return ret;
	};		   

	function createPngIcon(pngFileName, svg) {

		var stp = svg.append("svg:image");
		stp.attr("xlink:href", Drupal.settings.baseUrl+"/"+Drupal.settings.tripal_map.modulePath+"/theme/images/save_as_png.png")
			.attr('width', 35)
			.attr('height', 25)
			.attr("transform", "translate(" + 0 + "," + 0 +")")
			.attr("id", "save_to_png")
			.on("click", function(d) { tripalMap.onSaveToPngClick(svg, pngFileName);})
			.on("mouseover", function(d) { tripalMap.onSaveToPngMouseOver(svg);})
			.on("mouseout", function(d) { tripalMap.onSaveToPngMouseOut(svg);});

		return stp;
	};

	function drawChromosomes(uniqueMarkers, mapName, linkageGroupName, markerHighlight, markerData, chrFrameRefWidth,
		uniqueMarkersComparison, mapComparisonName, linkageGroupComparisonName, showComparison, pageFit, show, svg, pngIcon) {

		// set the initial Marker Position, the case where the page specifically highlights a marker
		var initMarkerHighlight = markerHighlight;

		// create chromosome frames for linkage groups
		var mapId = uniqueMarkers[0].featuremap_id;
		var chrFrameReference = new mapViewer.ChrFrame(mapViewer.OrientationEnum.LEFT, pageFit, show, mapName, mapId, 
				linkageGroupName, markerData, initMarkerHighlight);
		var chrFrameComparison = "";
		if (showComparison) {
			var mapComparisonId = uniqueMarkersComparison[0].featuremap_id;
			chrFrameComparison = new mapViewer.ChrFrame(mapViewer.OrientationEnum.RIGHT, pageFit, show, mapComparisonName, 
				mapComparisonId, linkageGroupComparisonName, markerData, initMarkerHighlight);
		}
		
		// draw markers on linkage groups
		// Reference linkage group
		var translateX = 0;
		var saveToPngHeight = pngIcon.node().getBoundingClientRect().height;
		var translateY = saveToPngHeight*2;
		chrFrameReference.setTranslate(translateX, translateY);
		svg = chrFrameReference.draw(svg);
		translateX = chrFrameRefWidth + 15; // take the statically calculated width based on full data set instead of zoomed section //svg.node().getBBox().width;

		// Comparison linkage group
		if (showComparison) {
			chrFrameComparison.setTranslate(translateX, translateY);
			svg = chrFrameComparison.draw(svg);
		}
				
		return svg;
	};		

	function drawLegend(markerData, svg, showComparison, pngIcon) {

		// draw the legend (including marker types that are configured as hidden) but only for the selected linkage groups	
		var legend = new mapViewer.Legend(markerData);
		var chrFrameRefHeight = svg.selectAll("#chrFrame_0").node().getBBox().height
		var chrFrameCompHeight = 0;
		if (showComparison) {
			chrFrameCompHeight = svg.selectAll("#chrFrame_1").node().getBBox().height;
		}
		var maxChrFrameHeight = chrFrameRefHeight > chrFrameCompHeight ? chrFrameRefHeight : chrFrameCompHeight;
		var saveToPngHeight = pngIcon.node().getBoundingClientRect().height;
		translateY = saveToPngHeight*2 + maxChrFrameHeight; 
		legend.setTranslate(0, translateY);
		svg = legend.draw(svg);
		return svg; 
	};

	}};
})(jQuery);




